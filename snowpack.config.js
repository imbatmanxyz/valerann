/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
  mount: {
    public: {url: '/', static: true},
    src: {url: '/dist'},
  },
  plugins: [
    '@snowpack/plugin-react-refresh',
    '@snowpack/plugin-dotenv',
    '@snowpack/plugin-typescript',
  ],
  routes: [
    /* Enable an SPA Fallback in development: */
    {"match": "routes", "src": ".*", "dest": "/index.html"},
  ],
  optimize: {
    /* Example: Bundle your final build: */
    // "bundle": true,
  },
  packageOptions: {
    /* ... */
    // to replace require method with import
    // rollup: {
    //   plugins: [
    //     require('rollup-plugin-re')({
    //       patterns: [
    //         {
    //           test: /require(\((["'])(?:\\\2|[^\n])*?\2\))/g,
    //           replace: 'import$1'
    //         }
    //       ]
    //     })
    //   ]
    // }
  },
  devOptions: {
    /* ... */
  },
  buildOptions: {
    /* ... */
  },
};
