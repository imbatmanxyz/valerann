import React from 'react';
import styled from 'styled-components';
import { useNavigate } from 'react-router-dom';

const Container = styled.nav`
  grid-area: nav;
  grid-column: 2 / 10;
`;

export function Navbar() {
  const navigate = useNavigate();

  return (
    <Container>
      <a href="" onClick={() => navigate('trajection')}>
        Vehicle Trajections
      </a>
    </Container>
  );
}
