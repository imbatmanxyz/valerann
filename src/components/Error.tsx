import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  grid-area: header;
`;

export const Error = ({ message }: any) => {
  return <Container>{message}</Container>;
};
