import React from 'react';
import styled from 'styled-components';
import { Navbar } from '../components/Navbar';

const Container = styled.div`
  display: grid;
  grid-template-areas:
    'nav'
    'header'
    'body'
    'footer';
  grid-template-columns: repeat(12, 1fr);
  grid-gap: 20px;
`;

export function Skeleton({ children }: any) {
  return (
    <Container>
      <Navbar />
      {children}
    </Container>
  );
}
