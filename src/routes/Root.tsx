import React from 'react';
import { Root } from '../pages/Root';

export const RootRoute = {
  path: '/',
  element: <Root />,
};
