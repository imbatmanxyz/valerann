import { useRoutes } from 'react-router-dom';
import { RootRoute } from './Root';
import { TrajectionRoute } from './Trajectory';

export const Routes = () => {
  return useRoutes([RootRoute, TrajectionRoute]);
};
