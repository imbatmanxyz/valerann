import React from 'react';
import { Trajection } from '../pages/Trajection';

export const TrajectionRoute = {
  path: 'trajection',
  element: <Trajection />,
};
