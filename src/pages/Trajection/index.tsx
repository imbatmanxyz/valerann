import React from 'react';
import { trajectories } from '../../adapters/data/api';
import { Map } from './Map';
import { Error } from '../../components/Error';
import { Skeleton } from '../../themes/layout';
import './styles.css';

export function Trajection() {
  const { isLoading, error, data } = trajectories();

  if (error) {
    return (
      <Skeleton>
        <Error message={error} />
      </Skeleton>
    );
  }

  return (
    <Skeleton>
      <div id="header">
        <h1>Trajection</h1>
      </div>
      <div id="body">
        <h1>Map</h1>
        {isLoading ? <div id="header">Loading....</div> : <Map data={data} />}
      </div>
    </Skeleton>
  );
}
