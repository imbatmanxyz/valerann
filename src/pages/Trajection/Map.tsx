import React, { useEffect, useRef } from 'react';
import mapboxgl from 'mapbox-gl';

mapboxgl.accessToken =
  'pk.eyJ1IjoiYWxhbi13cyIsImEiOiJja2t0eXR3eTkxMmloMndwY21na2N3anpxIn0.Er9BIAA4r26B62NnIskocg';

interface Trajections {
  color: number;
  points: [];
  size: number;
  speed: number;
  ts: number;
  vehicle_id: number;
}

function positionMap(data: Array<Trajections>): [number, number] {
  console.log(data);
  return [31.9904365726211, 34.7574869598805];
}

export function Map({ data }: any) {
  const map = useRef(null);

  useEffect(() => {
    const [lat, long] = positionMap(data.trajectories);

    new mapboxgl.Map({
      container: (map.current as unknown) as string, // not good
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [long, lat],
      zoom: 9,
    });
  }, []);

  return <div ref={map}></div>;
}
