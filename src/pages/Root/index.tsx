import React from 'react';
import { Skeleton } from '../../themes/layout';
import './styles.css';

export function Root() {
  return (
    <Skeleton>
      <div id="header">ROOT</div>
    </Skeleton>
  );
}
