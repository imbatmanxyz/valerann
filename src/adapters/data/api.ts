import { QueryClient, useQuery } from 'react-query';

export const queryClient = new QueryClient();

const JSON_HEADERS = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
};

export function vehicles() {
  return useQuery('vehicleData', () =>
    fetch('vehicles.json', {
      headers: JSON_HEADERS,
    }).then((res) => res.json()),
  );
}

export function trajectories() {
  return useQuery('trajectoryData', () =>
    fetch('trajectories.json', {
      headers: JSON_HEADERS,
    }).then((res) => res.json()),
  );
}
